import setuptools

setuptools.setup(
    name="plotting_interactive",
    version="0.1.0",
    author="Bojun Feng",
    author_email="davidfeng@uchicago.edu",
    description="Interactive functions to graph and analyze csv files from deeplabcut",
    url="https://bitbucket.org/EMK_Lab/DLC_csv_reader/",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=[
        'numpy',
        'pandas',
        'matplotlib',
    ],
)
