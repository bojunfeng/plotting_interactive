# Import relevant libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as pl

def format_csv(csvpath):
# Format the csv file
    
    # Load the csv file into a dataframe
    dataframe = pd.read_csv(csvpath)
    
    # Remove top lines before "bodyparts"
    while not "bodyparts" == dataframe.iloc[0][0]:
        dataframe = dataframe[1:]
        
    # Edit the header according to the given values
    underscore_list = ["_"] * len(dataframe.iloc[1])
    new_header = dataframe.iloc[0] + underscore_list + dataframe.iloc[1]
    dataframe.columns = new_header
    dataframe = dataframe[2:]
    return dataframe

def plotting_bodypart(file, pos, choiceslst):
# Plot a graph of bodypart's position in respect to time
    
    # Select the columns needed
    print("\nGotcha! " + pos.capitalize() + " it is!\n")
    posx = pos + "_x"
    posy = pos + "_y"
    poslike = pos + "_likelihood"
    
    # Convert data to float lists for graphing
    df = file[["bodyparts_coords", posx, posy, poslike]]
    xlst = df[posx].tolist()
    ylst = df[posy].tolist()
    likelst = df[poslike].tolist()
    timelst = df["bodyparts_coords"].tolist()
    for lst in [xlst, ylst, likelst, timelst]:
        for i in range(0, len(lst)):
            lst[i] = float(lst[i])
    maxt = max(timelst)
    for i in range(0, len(timelst)):
        lst[i] = lst[i]/max(timelst)
        
    # Plot the data
    print("plotting_bodypart data... ...")
    pl.scatter(x = xlst, y = ylst, s = likelst, c = timelst, cmap = pl.cm.get_cmap('RdYlBu'))
    pl.title("Rat " + pos + " Position", fontdict = {'fontsize': 17})
    cbar = pl.colorbar(orientation = "vertical", extend = "both", pad = 0.02, shrink = 1.1, aspect = 25)
    cbar.set_label(label = "Time", size=20)
    cbar.set_ticks([min(timelst), max(timelst)])
    cbar.set_ticklabels(["Start", "End"])
    pl.clim(min(timelst), max(timelst))
    pl.show()
    
    # Resurse the function if necessary
    choices = str(choiceslst).replace("'", "") + "\n"
    pos = input("\nWant another graph? Enter 1 to quit program.\nAvailable choices are: " +  choices)
    if pos == "1":
        print("\nBye! Have a nice day :)")
    else:
        plotting_bodypart(file, pos, choiceslst)

def choosing_bodypart(file, choiceslst):
# A recursive function for the user to choose bodypart to analyze
    
    # Edit the choicelist for the user to find the post
    string = "What is the body part you would like to analyze?\n"
    choices = str(choiceslst).replace("'", "")
    pos = input(string + "Available choices are: " +  choices + "\n(Enter 1 to quit program)\n")
    
    # Quit the program if 1 is entered and report an error if input not found
    if pos == "1":
        print("Bye! Have a nice day :)")
    if pos in choiceslst:
        plotting_bodypart(file, pos, choiceslst)
    else:
        print("Your input: " + pos + " can not be found in the csv!\n")
        choosing_bodypart(file, choiceslst)

def analyze_bodypart(csvpath, choiceslst):
# Load the csv file and format it with format_csv
    print("Initiating... ...\n")
    file = format_csv(csvpath)
    choosing_bodypart(file, choiceslst)
